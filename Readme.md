# Documentation
[http://kristijanhusak.github.io/laravel-form-builder/](http://kristijanhusak.github.io/laravel-form-builder/).


## Installation

``` json
{
     "repositories": [
        {
          "type": "git",
          "url": "http://192.168.178.50:7990/scm/mediapress/form-builder.git"
        }
      ],
      
      
      "require-dev": {
      //...
        "mediapress/form-builder":"*"
      },
}
```

run `composer update`

Then add Service provider to `config/app.php`

``` php
    'providers' => [
        // ...
        Mediapress\FormBuilder\FormBuilderServiceProvider::class
    ]
```


**Notice**: This package will add `laravelcollective/html` package and load aliases (Form, Html) if they do not exist in the IoC container


## Quick start

Create a form in function:

```php
<?php 

 private function createForm()
    {
        $data =
           [
                           ['first_name', 'url', ['rules' => 'required|min:5', 'label' => 'First Name']],
                           ['last_name', 'text', ['rules' => 'required|min:5', 'label' => 'Last Name']],
                           ['email', 'email', ['rules' => 'required|email']],
                           ['nerd_level', 'choice', [
                               'rules' => 'required',
                               'choices' => ['Rookie' => 'Rookie', 'Junior' => 'Junior', 'Senior' => 'Senior'],
                               'empty_value' => '=== Select Level ===',
                               'label' => 'Nerd Label',
                               'multiple' => true
                           ]],
                           [
                               'filter', 'tab',
                               [
                                   'tabs' => [
                                       [
                                           'label' => 'Deneme',
                                           'fields' =>
                                               [
                                                   ['deneme', 'text', ['rules' => 'required|min:5', 'label' => 'First Name']],
                                                   ['deneme2', 'text', ['rules' => 'required|min:5', 'label' => 'Last Name']],
                                               ]
                                       ],
                                       [
                                           'label' => 'Deneme2',
                                           'fields' =>
                                               [
                                                   ['deneme3', 'text', ['rules' => 'required|min:5', 'label' => 'Last Name']],
                                               ]
                                       ]
                                   ]
           
                               ]
           
                           ],
                           ['store', 'submit']
                       ];
        return $data;
    }
```



Print the form in view with `form()` helper function:

```html
<!-- resources/views/form.blade.php -->

@extends('app')

@section('content')
    {!! form($form) !!}
@endsection
```

Go to `/form`; above code will generate this html:

```html
<form method="POST" action="http://www.example.com" accept-charset="UTF-8">
    <input name="_token" type="hidden" value="nWXsDdNqHFiYWMNNb4BdfC1YKEoW0oLEyfKdQvvy">
    <div class="form-group">
        <label for="first_name" class="control-label required">First Name</label>
        <input class="form-control" required="required" minlength="5" name="first_name" type="text" id="first_name">
    </div>
    <div class="form-group">
        <label for="last_name" class="control-label required">Last Name</label>
        <input class="form-control" required="required" minlength="5" name="last_name" type="text" id="last_name">
    </div>
    <div class="form-group">
        <label for="email" class="control-label required">Email</label>
        <input class="form-control" required="required" name="email" type="email" id="email">
    </div>
    <div class="form-group">
        <label for="nerd_level" class="control-label required">Nerd Label</label>
        <select class="form-control" required="required" id="nerd_level" name="nerd_level">
            <option value="" selected="selected">=== Select Level ===</option>
            <option value="Rookie">Rookie</option>
            <option value="Junior">Junior</option>
            <option value="Senior">Senior</option>
        </select>
    </div>
    <button class="form-control" type="submit">Store</button>
</form>
```
