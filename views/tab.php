<?php if ($showLabel && $showField): ?>
<?php if ($options['wrapper'] !== false): ?>


<div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
    <?php endif; ?>

    <ul class="nav nav-tabs" role="tablist">
        <?php
        $i = 0;
        foreach ($options['tabs'] as $tab): ?>
            <li role="presentation" <?= (($i == 0) ? 'class="active"' : '') ?> ><a
                    href="#tab<?= $i ?>">   <?= $tab['label'] ?></a>
            </li>
            <?php
            $i++;
        endforeach; ?>
    </ul>

    <div class="tab-content">
        <?php if ($showField): ?>
        <?php
        $i = 0;
        foreach ($options['children'] as $tabDetail): ?>
            <div role="tabpanel" class="tab-pane fade in<?= (($i == 0) ? ' active' : '') ?>" id="tab<?= $i ?>">
                <?php foreach ((array)$tabDetail as $child): ?>
                   
                    <?= $child->render() ?>
                <?php endforeach; ?>
            </div>
            <?php
            $i++;
        endforeach; ?>
    </div>
<?php include 'help_block.php' ?>

<?php endif; ?>

    <?php include 'errors.php' ?>

    <?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
</div>
<?php endif; ?>
<?php endif; ?>
