<?php

namespace Mediapress\FormBuilder\Fields;

class InputType extends FormField
{
    /**
     * @return string
     */
    protected function getTemplate()
    {
        return 'text';
    }
}