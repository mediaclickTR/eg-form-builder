<?php

namespace Mediapress\FormBuilder\Fields;

use Mediapress\FormBuilder\FormCreator;

class TabField extends ParentType
{
    protected function getTemplate()
    {
        return 'tab';
    }

    protected function createChildren()
    {

        $this->form = $this->getClassFromOptions();

        foreach ($this->form as $form){
            if ($form->getFormOption('files')) {
                $this->parent->setFormOption('files', true);
            }
        }

        $model = $this->getOption($this->valueProperty);
        if ($this->isValidValue($model)) {
            foreach ($form->getFields() as $name => $field) {
                $field->setValue($this->getModelValueAttribute($model, $name));
            }
        }

        foreach ($this->form as $form){
          
            $this->children[] = $form->getFields();
        }

    }

    protected function getClassFromOptions()
    {
        if ($this->form instanceof Form) {
            return $this->form->setName($this->name);
        }


        $class = FormCreator::class;

        if (!class_exists($class)) {
            throw new \InvalidArgumentException(
                'Form class with name ' . $class . ' does not exist.'
            );
        }

        if (is_string($class)) {
            $options = [
                'model' => $this->parent->getModel(),
                'name' => $this->name,
                'language_name' => $this->parent->getLanguageName(),
                'field'=>'tab'
            ];

            if (!$this->parent->clientValidationEnabled()) {
                $options['client_validation'] = false;
            }

            if (!$this->parent->haveErrorsEnabled()) {
                $options['errors_enabled'] = false;
            }

            $formOptions = $options;
            $data = $this->parent->getData();

            return $this->parent->getFormBuilder()->createTab($this->options, $formOptions, $data);
        }

        if ($class instanceof Form) {
            $class->setName($this->name, false);
            $class->setModel($class->getModel() ?: $this->parent->getModel());

            if (!$class->getData()) {
                $class->addData($this->parent->getData());
            }

            if (!$class->getLanguageName()) {
                $class->setLanguageName($this->parent->getLanguageName());
            }

            if (!$this->parent->clientValidationEnabled()) {
                $class->setClientValidationEnabled(false);
            }

            if (!$this->parent->haveErrorsEnabled()) {
                $class->setErrorsEnabled(false);
            }

            return $class->setName($this->name);
        }

        throw new \InvalidArgumentException(
            'Class provided does not exist or it passed in wrong format.'
        );
    }
}