<?php
namespace Mediapress\FormBuilder;

class FormCreator extends Form
{
    public function buildFields($fields)
    {
        foreach ($fields as $field) {
            call_user_func_array([$this, 'add'], $field);
        }
    }
}