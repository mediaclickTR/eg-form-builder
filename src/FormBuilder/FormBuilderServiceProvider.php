<?php

namespace Mediapress\FormBuilder;


use Collective\Html\HtmlBuilder;
use Collective\Html\FormBuilder as LaravelForm;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class FormBuilderServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerHtmlIfNeeded();
        $this->registerFormIfHeeded();
        $this->registerFormHelper();

        $this->app->singleton('form-builder', function ($app) {
            return new FormBuilder($app, $app['form-helper']);
        });

        $this->app->alias('form-builder', 'Mediapress\FormBuilder\FormBuilder');


    }

    protected function registerFormHelper()
    {
        $this->app->singleton('form-helper', function ($app) {
            $configuration = $app['config']->get('form-builder');
            return new FormHelper($app['view'], $app['translator'], $configuration);
        });
        $this->app->alias('form-helper', 'Mediapress\FormBuilder\FormHelper');
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../../views', 'form-builder');
        $this->publishes([
            __DIR__ . '/../../views' => base_path('resources/views/vendor/form-builder'),
            __DIR__ . '/../../config/config.php' => config_path('form-builder.php')
        ], 'form-builder');
    }

    public function provides()
    {
        return ['form-builder'];
    }

    /**
     * Add Laravel Html to container if not already set
     */
    private function registerHtmlIfNeeded()
    {
        if (!$this->app->offsetExists('html')) {
            $this->app->singleton('html', function ($app) {
                return new HtmlBuilder($app['url'], $app['view']);
            });
            if (!$this->aliasExists('Html')) {
                AliasLoader::getInstance()->alias(
                    'Html',
                    'Collective\Html\HtmlFacade'
                );
            }
        }
    }

    /**
     * Add Laravel Form to container if not already set
     */
    private function registerFormIfHeeded()
    {
        if (!$this->app->offsetExists('form')) {
            $this->app->singleton('form', function ($app) {
                if (substr(Application::VERSION, 0, 3) == '5.2') {
                    $form = new LaravelForm($app['html'], $app['url'], $app['view'], $app['session.store']->getToken());
                } else {
                    $form = new LaravelForm($app['html'], $app['url'], $app['session.store']->getToken());
                }
                return $form->setSessionStore($app['session.store']);
            });
            if (!$this->aliasExists('Form')) {
                AliasLoader::getInstance()->alias(
                    'Form',
                    'Collective\Html\FormFacade'
                );
            }
        }
    }

    /**
     * Check if an alias already exists in the IOC
     * @param $alias
     * @return bool
     */
    private function aliasExists($alias)
    {
        return array_key_exists($alias, AliasLoader::getInstance()->getAliases());
    }

}